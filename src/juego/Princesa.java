package juego;

import java.awt.Image;
import entorno.Entorno;
import entorno.Herramientas;


public class Princesa {
    private double x;
    private double y;
    private double velocidad;
    private double angulo;
    private double velocidadSalto;
    private boolean saltando;
    private String direccion;
    private Image prinderecha;
    private Image prinizquierda;
    private Image prinderechaSaltando;
    private Image prinizquierdaSaltando;
    private boolean teclaIzquierdaPresionada;
    private boolean teclaDerechaPresionada;
    private double gravedad = 5.5;
    private boolean enPlataforma;
    private double anchoPrincesa = 50; // Ancho aproximado de la princesa
    private double altoPrincesa = 50; // Alto aproximado de la princesa

    public Princesa(int x, double y, double velocidad) {
        this.x = x;
        this.y = y;
        this.velocidad = 3.0;
        this.angulo = 0;
        this.velocidadSalto = 40;
        this.saltando = false;
        this.prinderecha = Herramientas.cargarImagen("Multimedia/princesaderecha.png");
        this.prinizquierda = Herramientas.cargarImagen("Multimedia/princesaizquierda.png");
        this.prinderechaSaltando = Herramientas.cargarImagen("Multimedia/prinsaltder.png");
        this.prinizquierdaSaltando = Herramientas.cargarImagen("Multimedia/princesasalto.png");

        this.direccion = "";
        this.teclaIzquierdaPresionada = false;
        this.teclaDerechaPresionada = false;
        this.enPlataforma = false;
    }

    public void moverHaciaIzquierda(boolean presionada) {
        teclaIzquierdaPresionada = presionada;
        if (presionada) {
            x -= velocidad;
            direccion = "izquierda";
        }
        if (x < 25) {
            x = 25;
        }
    }

    public void moverHaciaDerecha(boolean presionada) {
        teclaDerechaPresionada = presionada;
        if (presionada) {
            x += velocidad;
            direccion = "derecha";
        }
        if (x > 775) {
            x = 775;
        }
    }

    public void saltar() {
        if (enPlataforma) {
            saltando = true;
            enPlataforma = false;
            Herramientas.play("Multimedia/mariobros-jump.wav"); // Reproduce el sonido de salto
        }
    }

    public void actualizarPosicion(Piso[] pisos) {
        if (saltando) {
            y -= velocidadSalto;
            velocidadSalto -= gravedad;
            if (velocidadSalto <= 0) {
                saltando = false;
                velocidadSalto = 40;
            }
        } else {
            y += gravedad;
        }

        enPlataforma = false;

        for (Piso piso : pisos) {
            for (int i = 0; i < piso.getBloques().length; i++) {
                Bloque bloque = piso.getBloques()[i];
                if (bloque != null && colisionaConBloque(bloque)) {
                    if (bloque.getY() > y) {
                        y = bloque.getY() - altoPrincesa;
                        enPlataforma = true;
                    } else if (saltando && bloque.isDestructible()) {
                        piso.getBloques()[i] = null;
                        saltando = false;
                        velocidadSalto = 40;
                        Herramientas.play("Multimedia/rompe.wav");
                    } else if (!bloque.isDestructible() && bloque.getY() < y) {
                        y = bloque.getY() + altoPrincesa;
                        saltando = false;
                        velocidadSalto = 40;
                    } else if (x < bloque.getX() && x + anchoPrincesa > bloque.getX() - anchoPrincesa / 2) {
                        x = bloque.getX() - anchoPrincesa;
                    } else if (x > bloque.getX() && x - anchoPrincesa < bloque.getX() + anchoPrincesa / 2) {
                        x = bloque.getX() + anchoPrincesa;
                    }
                }
            }
        }

        if (y > 600) {
            y = 600;
        }
    }

    private boolean colisionaConBloque(Bloque bloque) {
        return x + anchoPrincesa / 2 > bloque.getX() - bloque.getAncho() / 2 &&
               x - anchoPrincesa / 2 < bloque.getX() + bloque.getAncho() / 2 &&
               y + altoPrincesa / 2 > bloque.getY() - bloque.getAlto() / 2 &&
               y - altoPrincesa / 2 < bloque.getY() + bloque.getAlto() / 2;
    }

    public void dibujar(Entorno entorno) {
        if (saltando) {
            if (direccion.equals("izquierda")) {
                entorno.dibujarImagen(prinizquierdaSaltando, x, y, angulo, 1);
            } else {
                entorno.dibujarImagen(prinderechaSaltando, x, y, angulo, 1);
            }
        } else if (!enPlataforma) {
            if (direccion.equals("izquierda")) {
                entorno.dibujarImagen(prinizquierdaSaltando, x, y, angulo, 1);
            } else {
                entorno.dibujarImagen(prinderechaSaltando, x, y, angulo, 1);
            }
        } else {
            if (direccion.equals("izquierda")) {
                entorno.dibujarImagen(prinizquierda, x, y, angulo, 1);
            } else {
                entorno.dibujarImagen(prinderecha, x, y, angulo, 1);
            }
        }
    }

    public boolean estaSaltando() {
        return saltando;
    }

    public void detenerSalto() {
        saltando = false;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public String getDireccion() {
        return direccion;
    }

    public double getVelocidadVertical() {
        return velocidadSalto;
    }

    public int getAncho() {
        return (int) anchoPrincesa;
    }

    public int getAlto() {
        return (int) altoPrincesa;
    }

    public boolean estaEnPlataforma() {
        return enPlataforma;
    }
}
