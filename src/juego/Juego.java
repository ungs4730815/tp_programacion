package juego;

import entorno.Entorno;
import entorno.Herramientas;
import entorno.InterfaceJuego;
import java.util.ArrayList;

public class Juego extends InterfaceJuego {
    private Entorno entorno;
    private Fondo fondo; // El fondo
    private Princesa princesa; //La Princesa
    private ArrayList<Dinosaurio> dinosaurios; // Arreglo de Dinosaurios
    private ArrayList<BalaDino> bombas; //Arreglo de las Balas de Dinosaurios
    private Piso pisoBase; // El Piso Base( Indestructible)
    private Piso[] pisos; // Los Pisos
    private ArrayList<BalasPrincesa> balas; // Arreglo de Balas de Princesa
    private ArrayList<BalaDino> bombasRemover; //Arreglo Que Cuanta Las Balas de Dinosaurios
    private int enemigosEliminados; // Contador de enemigos eliminados
    private int plataformasSubidas; // Contador de plataformas subidas


    public Juego() {
        this.entorno = new Entorno(this, "Super Elizabeth Sis, Volcano Edition", 800, 700);
        
        this.fondo = new Fondo("Multimedia/fondo.png", entorno.getWidth() / 2, (entorno.getHeight() / 3) + 50);
        
        this.pisoBase = new Piso(16, 650, true); // Piso inferior, completamente indestructible
        inicializarDinosaurios();
        this.pisos = new Piso[]{
            pisoBase,
            new Piso(16, 490, false),
            new Piso(16, 330, false),
            new Piso(16, 170, false),
            new Piso(16, 10, false)
        };

        this.princesa = new Princesa(400, pisoBase.getYInicio(), 2.0); //Inicializa a la Princesa
        this.balas = new ArrayList<>(); //Inicializa las balas
        this.bombas = new ArrayList<>(); // Inicializa las Bombas
        this.bombasRemover = new ArrayList<>();
        this.enemigosEliminados = 0; // Inicializa contador de enemigos eliminados
        this.plataformasSubidas = 0; // Inicializa contador de plataformas subidas

        this.entorno.iniciar();
    }

    private void inicializarDinosaurios() {
        this.dinosaurios = new ArrayList<>();
        int BajadaPisos = (int) pisoBase.getYInicio() - 210;
        this.dinosaurios.add(new Dinosaurio(0, pisoBase.getYInicio() - 50));
        this.dinosaurios.add(new Dinosaurio(entorno.ancho(), pisoBase.getYInicio() - 50));
        for (int i = 0; i < 6; i++) {
            this.dinosaurios.add(new Dinosaurio(Math.random() * entorno.ancho(), BajadaPisos));
            this.dinosaurios.add(new Dinosaurio(Math.random() * entorno.ancho(), BajadaPisos));
            BajadaPisos = BajadaPisos - 160;
        }
    }

    public void tick() {
    	
        fondo.dibujar(entorno);
        
        princesa.dibujar(entorno);
        
        for (Piso piso : pisos) {
            piso.dibujar(entorno);
        }

        // Dibujar y mover dinosaurios
        for (Dinosaurio dino : this.dinosaurios) {
            dino.dibujarse(this.entorno);
            dino.mover();
            if (princesa.getY() == dino.getY() && this.bombas.size() < 2) {
                if (princesa.getX() > entorno.ancho() /2) {
                    this.bombas.add(dino.lanzarBomba(princesa, entorno, "derecha"));
                } else {
                    this.bombas.add(dino.lanzarBomba(princesa, entorno, "izquierda"));
                }
            }
        }

        // Manejo del movimiento de la princesa
        if (entorno.estaPresionada(entorno.TECLA_IZQUIERDA)) {
            princesa.moverHaciaIzquierda(true);
        } else {
            princesa.moverHaciaIzquierda(false);
        }

        if (entorno.estaPresionada(entorno.TECLA_DERECHA)) {
            princesa.moverHaciaDerecha(true);
        } else {
            princesa.moverHaciaDerecha(false);
        }

        if (entorno.estaPresionada(entorno.TECLA_ARRIBA)) {
            princesa.saltar();
        }

        princesa.actualizarPosicion(pisos);

        // Disparo de la bala (solo cuando está en una plataforma(
        if (entorno.sePresiono(entorno.TECLA_ESPACIO) && balas.size() == 0 && princesa.estaEnPlataforma()) {
            String direccionBala = princesa.getDireccion(); // Obtener la dirección de la princesa
            BalasPrincesa bala = new BalasPrincesa(princesa.getX(), princesa.getY(), direccionBala, 5);
            balas.add(bala);
            Herramientas.play("Multimedia/balaPrin.wav");
        }

        // Dibujar balas
        for (int i = 0; i < balas.size(); i++) {
            BalasPrincesa bala = balas.get(i);
            bala.mover();
            if (bala.fueraDePantalla(entorno)) {
                balas.remove(i);
                i--;
            } else {
                bala.dibujarse(entorno);
            }
        }

        // Dibujar y mover bombas
        for (int i = 0; i < bombas.size(); i++) {
            bombas.get(i).dibujarse(entorno);
            bombas.get(i).mover();
            if (bombas.get(i).fueraDePantalla(entorno)) {
                bombasRemover.add(bombas.get(i));
            }
        }
        this.bombas.removeAll(bombasRemover);
    
	 // Dibujar contadores de enemigos eliminados y plataformas subidas
	    entorno.cambiarFont("Impact", 20, java.awt.Color.GREEN);
	    entorno.escribirTexto("ELIMINADOS: " + enemigosEliminados, 10, 55);
	    entorno.escribirTexto("PISO: " + plataformasSubidas, 10, 75);
	
	    // Actualiza contador de plataformas subidas
	    actualizarPlataformasSubidas();
	    }
	
	    private void actualizarPlataformasSubidas() {
	    	int nivelActual = (int) (princesa.getY() / 160);
	    	plataformasSubidas = 4- nivelActual; 
		}

    public static void main(String[] args) {
        Juego juego = new Juego();
    }
}
