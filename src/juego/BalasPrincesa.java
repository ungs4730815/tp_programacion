package juego;

import entorno.Entorno;
import entorno.Herramientas;
import java.awt.Image;

public class BalasPrincesa {
    private double x;
    private double y;
    private double velocidad;
    private String direccion;
    private Image imagenDerecha;
    private Image imagenIzquierda;
    private boolean activa;

    public BalasPrincesa(double x, double y, String direccion, double velocidad) {
        this.x = x;
        this.y = y;
        this.velocidad = velocidad;
        this.direccion = direccion;
        this.imagenDerecha = Herramientas.cargarImagen("Multimedia/granada_der.png");
        this.imagenIzquierda = Herramientas.cargarImagen("Multimedia/granada_izq.png");
        this.activa = true;
    }

    public void dibujarse(Entorno entorno) {
        if (activa) {
            if ("derecha".equals(direccion)) {
                entorno.dibujarImagen(this.imagenDerecha, this.x, this.y, 0);
            } else if ("izquierda".equals(direccion)) {
                entorno.dibujarImagen(this.imagenIzquierda, this.x, this.y, 0);
            }
        }
    }

    public void mover() {
        if (activa) {
            if ("derecha".equals(direccion)) {
                this.x += this.velocidad;
            } else if ("izquierda".equals(direccion)) {
                this.x -= this.velocidad; 
            }
        }
    }

    public boolean fueraDePantalla(Entorno entorno) {
        return this.x < 0 || this.x > entorno.ancho();
    }

    public boolean colisionaConBloque(Bloque bloque) {
        double anchoBala = 20; // Ancho aproximado de la bala
        double altoBala = 10; // Alto aproximado de la bala
        double anchoBloque = bloque.getAncho();
        double altoBloque = bloque.getAlto();
        
        if (x + anchoBala / 2 > bloque.getX() - anchoBloque / 2 &&
               x - anchoBala / 2 < bloque.getX() + anchoBloque / 2 &&
               y + altoBala / 2 > bloque.getY() - altoBloque / 2 &&
               y - altoBala / 2 < bloque.getY() + altoBloque / 2) {
            return true;
        }
        return false;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public boolean isActiva() {
        return activa;
    }

    public void desactivar() {
        this.activa = false;
    }
}