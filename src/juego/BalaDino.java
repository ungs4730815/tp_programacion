package juego;

import entorno.Entorno;
import entorno.Herramientas;
import java.awt.Image;

public class BalaDino {
    private double x;
    private double y;
    private double velocidad;
    private String direccion;
    private Image imagen;
    private double angulo;

    public BalaDino(double x, double y,String direccion,double velocidad) {
        this.x = x;
        this.y = y;
        this.velocidad = velocidad;
        this.angulo = 0;
        this.direccion = direccion;
        this.imagen = Herramientas.cargarImagen("Multimedia/BalaDino.png");
    }
    
    public void dibujarse(Entorno entorno) {
           entorno.dibujarImagen(imagen, this.x, this.y, this.angulo, 0.05);
    }

    public void mover() {
        if (this.direccion.equals("derecha")) {
            this.x += this.velocidad;
        } else {
            this.x -= this.velocidad;
        }
    }
    

    public boolean fueraDePantalla(Entorno entorno) {
        return this.y > entorno.alto() || this.x < 0 || this.x > entorno.ancho();
    }

    public boolean chocasteCon(Princesa princesa) {
        return (this.x + this.imagen.getWidth(null) / 2 > princesa.getX() - princesa.getAncho() / 2) &&
               (this.x - this.imagen.getWidth(null) / 2 < princesa.getX() + princesa.getAncho() / 2) &&
               (this.y + this.imagen.getHeight(null) / 2 > princesa.getY() - princesa.getAlto() / 2) &&
               (this.y - this.imagen.getHeight(null) / 2 < princesa.getY() + princesa.getAlto() / 2);
    }

}
