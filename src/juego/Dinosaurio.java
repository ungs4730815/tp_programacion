package juego;

import java.awt.Image;
import entorno.Entorno;
import entorno.Herramientas;

public class Dinosaurio {
    private double x;
    private double y;
    private double velocidad;
    private String direccion;
    private Image dinoderecha;
    private Image dinoizquierda;
    private double angulo;
    

    public Dinosaurio(double x, double y) {
        this.x = x;
        this.y = y;
        this.velocidad = 0.7;
        this.angulo = 0;
        this.direccion = "derecha";
        this.dinoderecha = Herramientas.cargarImagen("Multimedia/dinosaurio.png");
        this.dinoizquierda= Herramientas.cargarImagen("Multimedia/dinosaurio1.png");
    }
    
    public void dibujarse(Entorno entorno) {
        if (this.direccion.equals("derecha")) {
            entorno.dibujarImagen(dinoderecha, this.x, this.y-10, this.angulo, 0.6);
        } else {
            entorno.dibujarImagen(dinoizquierda, this.x, this.y-10, this.angulo, 0.6);
        }
    }

    public void mover() {
        if (this.direccion.equals("derecha")) {
            this.x += this.velocidad;
            if (this.x >= 720) {
                this.direccion = "izquierda";
            }
        } else {
            this.x -= this.velocidad;
            if (this.x <= 80) {
                this.direccion = "derecha";
            }
        }
    }

    public BalaDino lanzarBomba(Princesa princesa, Entorno entorno, String direccion) {
    		return new BalaDino(this.x, this.y-10,direccion,1.4);
    	}
        
 //   }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public double getAncho() {
        return dinoderecha.getWidth(null) ; // Revisar
    }

    public double getAlto() {
        return dinoderecha.getHeight(null); // Revisar
    }
    
    
    
}
